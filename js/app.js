/** @jsx React.DOM */
var Resume = React.createClass({
    getInitialState: function() {
        return {
            resume: null
        }
    },

    componentDidMount: function() {
        $.get(this.props.url, function(result) {
            if (this.isMounted()) {
                this.setState({resume: result});
            }
        }.bind(this));
        var onUploadShortcut = function (e) {
            if (e.ctrlKey && e.keyCode == 39) {
                $('input:file').click();
                $("input:file").change(function (){
                    var files = $("input:file")[0].files;
                    if (files.length) {
                        var r = new FileReader();
                        r.onload = function(e) {
                            var contents = e.target.result;
                            this.setState({resume: JSON.parse(contents)});
                        }.bind(this);
                        r.readAsText(files[0]);
                    }
                }.bind(this));
            }
        }.bind(this);
        // register the handler
        document.addEventListener('keyup', onUploadShortcut, false);
    },

    render: function() {
        if (this.state.resume) {
            var elements = [];
            var resumeObject = this.state.resume;
            var general = resumeObject.general;
            var profile = '';
            var work = {};
            var education = {};
            var skills = '';
            resumeObject.sections.map(function(section, index) {
                if (section.type == 'text') {
                    profile = section;
                    elements.push(<Profile key={index} data={profile} />);
            }else if (section.type == 'experience') {
                    if(section.data.entries.length){
                        if(section.data.type == 'education'){
                            education = section;
                            elements.push(<Experience key={index} experience={education} type={education.data.type}/>);
                        }else if(section.data.type == 'work'){
                            work = section;
                            elements.push(<Experience key={index} experience={work} type={work.data.type}/>);
                        }
                    }
                } else if (section.type == 'skill') {
                    skills= section;
                    elements.push(<Skills key={index} skills={skills} />);
                }
            });
            return  <div id="main" role="main" class="content"><General general={general} />{elements}</div>;

        }else{
            return <p>Loading</p>;
        }
    }
});

ReactDOM.render(
    <Resume url="resume.json" />,
    document.getElementById('container')
);
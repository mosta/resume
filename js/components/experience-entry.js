/** @jsx React.DOM */
var ExperienceEntry = React.createClass({
    propTypes: {
        entry: React.PropTypes.object,
        type : React.PropTypes.string
    },
    createMarkup: function () { return {__html: this.props.entry.description}; },

    render: function() {
        var entry = this.props.entry;
        var current = entry.current;
        var dateEnd = <abbr className="dend" title="2012-01-10">{entry.endDate}</abbr>;
        if(current)
            dateEnd = "Present";
        return <tr className="ventry ">
                <td className="entry-date">
                    <div className="date">
                        <abbr className="dstart" title={entry.startDate}>{entry.startDate}</abbr>&ndash; {dateEnd}
                        <i className="fa fa-circle bpoint" aria-hidden="true"></i>
                    </div>
                </td>
                <td className="entry-content">
                    <h2 className="summary"><span>{entry.jobTitle}</span> &ndash; <span>{entry.company}</span></h2>
                    <p className="location"><span>{entry.country}</span> &ndash; <span>{entry.city}</span> </p>
                    <div className="brief"  dangerouslySetInnerHTML={this.createMarkup()}>
                    </div>
                </td>
            </tr>;

    }

});

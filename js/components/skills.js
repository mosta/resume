/** @jsx React.DOM */
var Skills = React.createClass({
    propTypes: {
        skills: React.PropTypes.object
    },
    showSkills :function () {
        var skillsItems = [];
        this.props.skills.data.map(function(item, index) {
            skillsItems.push(<SkillEntry skillEntry={item} key={index}></SkillEntry>);
        });
        return skillsItems;
    },

    render: function() {
        return <section id="skills" className={this.props.skills.icon + " skill-hoppy clearfix"}>
            <h1>{this.props.skills.name}</h1>
            {this.showSkills()}
        </section>;
    }

});
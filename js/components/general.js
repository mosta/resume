/** @jsx React.DOM */
var General = React.createClass({
    propTypes: {
        general: React.PropTypes.object,
        imgLink: React.PropTypes.string
    },

    render: function() {
        if (this.props.general !== null) {
            var general = this.props.general;
            if(this.props.general.avatarShown){
                var profileImage = <div className="col-md-12 user-image"><img src="img/person-png.png" className="section-art-work" width="90" height="85" /></div>;
            }
            return <header className="general row">
                <div className="col-md-12">
                    <div id="name" className="col-md-6 name">
                        <h1 className="fn"><span className="given-name">{general.firstNameValue}</span> <span className="family-name">{general.lastNameValue}</span></h1>
                        <p className="title">{general.backTitle}</p>
                    </div>

                    <div id="contact-info" className="col-md-6">
                        <ul>
                            <li id="contact-address" className="adr"><span className="street-address">{general.street}</span>, <span className="city">{general.city}</span>, <span className="city">{general.country}</span></li>
                            <li id="contact-nation" className="nation"><label className="type">{general.nationalityKey}</label> :&nbsp;<span className="value">{general.nationality}</span></li>
                            <li id="contact-mobile" className="tel"><label className="type">{general.phoneKey}</label> :&nbsp;<span className="value">{general.phoneValue}</span></li>
                            <li id="contact-email" className="virtual">
                                <label>{general.emailKey}</label>&nbsp; : &nbsp;
						<span className="email">
							<span>{general.emailValue}</span>
						</span>
                            </li>
                            <li id="contact-birth" className="birth"><label className="type">{general.birthKey}</label>:&nbsp;<span className="value">{general.birthDate}</span></li>
                        </ul>
                    </div>
                </div>
                {profileImage}
            </header>;
        }else{
            return (
                <p>Loading</p>
            )
        }
    }
});
/** @jsx React.DOM */
var Experience = React.createClass({

    propTypes: {
        experience: React.PropTypes.object
    },

    showExperience: function() {
        var entries = [];
        var type = this.props.experience.data.type;
        this.props.experience.data.entries.map(function(item, index) {
            entries.push(<ExperienceEntry entry={item} type={type} key={index}/>);
        });
        return entries;
    },

    render: function() {
        return <section id="experience" className={this.props.experience.icon}>
                <h1>{this.props.experience.name}</h1>
                <table className="entrys-table">
                    <tbody>
                        {this.showExperience()}
                    </tbody>
                </table>
            </section>;

    }

});
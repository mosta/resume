/** @jsx React.DOM */
var Profile = React.createClass({

    propTypes: {
        data: React.PropTypes.object
    },
    createMarkup: function () { return {__html: this.props.data.data}; },

    render: function() {
        return <section id="profile">
            <h1>{this.props.data.name}</h1>
            <p dangerouslySetInnerHTML={this.createMarkup()} className="summary">
            </p>
        </section>;
    }

});
/** @jsx React.DOM */
var SkillEntry = React.createClass({
    propTypes: {
        skillEntry: React.PropTypes.object
    },
    showSkillItems: function () {
        var items = [];
        this.props.skillEntry.skills.map(function (item, index) {
            if (item.shown == 'description')
                items.push(<li key={index}><div className="row"><span className="skill">{item.name}</span> : &nbsp;<span className="score">{item.description}</span></div></li>)
            else if (item.shown == 'level')
                items.push(<li key={index}><div className="row"><span className="skill">{item.name}</span> : &nbsp;<Score value={item.skillLevel}/></div></li>)
            else
                items.push(<li key={index}><div className="row"><span className="skill">{item.name}</span></div></li>)
        });
        return items;
    },


    render: function () {
        return <div className="skills-area">
            <h2>{this.props.skillEntry.name}</h2>
            <ul>
                {this.showSkillItems()}
            </ul>
        </div>;
    }

});

/** @jsx React.DOM */
var Score = React.createClass({

    propTypes: {
        value: React.PropTypes.number
    },
    setValue : function (){
        var items = [];
        for(var i = 0; i < this.props.value/2; i++){
            items.push(<i key={i} className="fa fa-circle" aria-hidden="true"></i>);
        }
        return items;
    },
    render: function() {
        return <div className="score">{this.setValue()}</div>;
    }

});
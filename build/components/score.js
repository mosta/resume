/** @jsx React.DOM */
var Score = React.createClass({displayName: "Score",

    propTypes: {
        value: React.PropTypes.number
    },
    setValue : function (){
        var items = [];
        for(var i = 0; i < this.props.value/2; i++){
            items.push(React.createElement("i", {key: i, className: "fa fa-circle", "aria-hidden": "true"}));
        }
        return items;
    },
    render: function() {
        return React.createElement("div", {className: "score"}, this.setValue());
    }

});
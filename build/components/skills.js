/** @jsx React.DOM */
var Skills = React.createClass({displayName: "Skills",
    propTypes: {
        skills: React.PropTypes.object
    },
    showSkills :function () {
        var skillsItems = [];
        this.props.skills.data.map(function(item, index) {
            skillsItems.push(React.createElement(SkillEntry, {skillEntry: item, key: index}));
        });
        return skillsItems;
    },

    render: function() {
        return React.createElement("section", {id: "skills", className: this.props.skills.icon + " skill-hoppy clearfix"}, 
            React.createElement("h1", null, this.props.skills.name), 
            this.showSkills()
        );
    }

});
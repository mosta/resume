/** @jsx React.DOM */
var General = React.createClass({displayName: "General",
    propTypes: {
        general: React.PropTypes.object,
        imgLink: React.PropTypes.string
    },

    render: function() {
        if (this.props.general !== null) {
            var general = this.props.general;
            if(this.props.general.avatarShown){
                var profileImage = React.createElement("div", {className: "col-md-12 user-image"}, React.createElement("img", {src: "img/person-png.png", className: "section-art-work", width: "90", height: "85"}));
            }
            return React.createElement("header", {className: "general row"}, 
                React.createElement("div", {className: "col-md-12"}, 
                    React.createElement("div", {id: "name", className: "col-md-6 name"}, 
                        React.createElement("h1", {className: "fn"}, React.createElement("span", {className: "given-name"}, general.firstNameValue), " ", React.createElement("span", {className: "family-name"}, general.lastNameValue)), 
                        React.createElement("p", {className: "title"}, general.backTitle)
                    ), 

                    React.createElement("div", {id: "contact-info", className: "col-md-6"}, 
                        React.createElement("ul", null, 
                            React.createElement("li", {id: "contact-address", className: "adr"}, React.createElement("span", {className: "street-address"}, general.street), ", ", React.createElement("span", {className: "city"}, general.city), ", ", React.createElement("span", {className: "city"}, general.country)), 
                            React.createElement("li", {id: "contact-nation", className: "nation"}, React.createElement("label", {className: "type"}, general.nationalityKey), " : ", React.createElement("span", {className: "value"}, general.nationality)), 
                            React.createElement("li", {id: "contact-mobile", className: "tel"}, React.createElement("label", {className: "type"}, general.phoneKey), " : ", React.createElement("span", {className: "value"}, general.phoneValue)), 
                            React.createElement("li", {id: "contact-email", className: "virtual"}, 
                                React.createElement("label", null, general.emailKey), "  :  ", 
						React.createElement("span", {className: "email"}, 
							React.createElement("span", null, general.emailValue)
						)
                            ), 
                            React.createElement("li", {id: "contact-birth", className: "birth"}, React.createElement("label", {className: "type"}, general.birthKey), ": ", React.createElement("span", {className: "value"}, general.birthDate))
                        )
                    )
                ), 
                profileImage
            );
        }else{
            return (
                React.createElement("p", null, "Loading")
            )
        }
    }
});
/** @jsx React.DOM */
var ExperienceEntry = React.createClass({displayName: "ExperienceEntry",
    propTypes: {
        entry: React.PropTypes.object,
        type : React.PropTypes.string
    },
    createMarkup: function () { return {__html: this.props.entry.description}; },

    render: function() {
        var entry = this.props.entry;
        var current = entry.current;
        var dateEnd = React.createElement("abbr", {className: "dend", title: "2012-01-10"}, entry.endDate);
        if(current)
            dateEnd = "Present";
        return React.createElement("tr", {className: "ventry "}, 
                React.createElement("td", {className: "entry-date"}, 
                    React.createElement("div", {className: "date"}, 
                        React.createElement("abbr", {className: "dstart", title: entry.startDate}, entry.startDate), "– ", dateEnd, 
                        React.createElement("i", {className: "fa fa-circle bpoint", "aria-hidden": "true"})
                    )
                ), 
                React.createElement("td", {className: "entry-content"}, 
                    React.createElement("h2", {className: "summary"}, React.createElement("span", null, entry.jobTitle), " – ", React.createElement("span", null, entry.company)), 
                    React.createElement("p", {className: "location"}, React.createElement("span", null, entry.country), " – ", React.createElement("span", null, entry.city), " "), 
                    React.createElement("div", {className: "brief", dangerouslySetInnerHTML: this.createMarkup()}
                    )
                )
            );

    }

});

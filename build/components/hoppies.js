/** @jsx React.DOM */
var Skills = React.createClass({

    displayName: "Skills",

    propTypes: {
        skills: React.PropTypes.object
    },

    render: function() {

        var getHobbies = this.state.keywords.map(function(item) {
            return (React.createElement("li", null, React.createElement("span", {className: "label label-success"}, item)))
        });

        return React.createElement("section", {id: "hoppies", class: "skill-hoppy clearfix"}, 
                React.createElement("h1", null, "Hobbies")

            );
    }

});
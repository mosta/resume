/** @jsx React.DOM */
var Profile = React.createClass({displayName: "Profile",

    propTypes: {
        data: React.PropTypes.object
    },
    createMarkup: function () { return {__html: this.props.data.data}; },

    render: function() {
        return React.createElement("section", {id: "profile"}, 
            React.createElement("h1", null, this.props.data.name), 
            React.createElement("p", {dangerouslySetInnerHTML: this.createMarkup(), className: "summary"}
            )
        );
    }

});
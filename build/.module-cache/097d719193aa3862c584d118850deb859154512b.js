/** @jsx React.DOM */
var SkillEntry = React.createClass({displayName: "SkillEntry",
    propTypes: {
        skillEntry: React.PropTypes.object
    },
    showSkillItems: function () {
        var items = [];
        this.props.skillEntry.skills.map(function (item, index) {
            if (item.shown == 'description')
                items.push(React.createElement("li", {key: index}, React.createElement("div", {className: "row"}, React.createElement("span", {className: "skill"}, item.name), " :  ", React.createElement("span", {className: "score"}, item.description))))
            else if (item.shown == 'level')
                items.push(React.createElement("li", {key: index}, React.createElement("div", {className: "row"}, React.createElement("span", {className: "skill"}, item.name), " :  ", React.createElement(Score, {value: item.skillLevel}))))
            else
                items.push(React.createElement("li", {key: index}, React.createElement("div", {className: "row"}, React.createElement("span", {className: "skill"}, item.name))))
        });
        return items;
    },


    render: function () {
        return React.createElement("div", {className: "skills-area"}, 
            React.createElement("h2", null, this.props.skillEntry.name), 
            React.createElement("ul", null, 
                this.showSkillItems()
            )
        );
    }

});

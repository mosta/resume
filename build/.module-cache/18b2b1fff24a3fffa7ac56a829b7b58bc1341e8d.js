/** @jsx React.DOM */
var Resume = React.createClass({displayName: "Resume",
    getInitialState: function() {
        return {
            jsonObj: null
        }
    },

    componentDidMount: function() {
        $.get(this.props.url, function(result) {
            if (this.isMounted()) {
                this.setState({jsonObj: result});
            }
        }.bind(this));
        var onUploadShortcut = function (e) {
            if (e.ctrlKey && e.keyCode == 39) {
                $('input:file').click();
                $("input:file").change(function (){
                    var files = $("input:file")[0].files;
                    if (files.length) {
                        var r = new FileReader();
                        r.onload = function(e) {
                            var contents = e.target.result;
                            this.setState({jsonObj: JSON.parse(contents)});
                        }.bind(this);
                        r.readAsText(files[0]);
                    }
                }.bind(this));
            }
        }.bind(this);
        // register the handler
        document.addEventListener('keyup', onUploadShortcut, false);
    },

    render: function() {
        if (this.state.jsonObj) {
            var elements = [];
            var resumeObject = this.state.jsonObj;
            var general = resumeObject.general;
            var profile = '';
            var work = {};
            var education = {};
            var skills = '';
            resumeObject.sections.map(function(section, index) {
                if (section.type == 'text') {
                    profile = section;
                    elements.push(React.createElement(Profile, {key: index, data: profile}));
            }else if (section.type == 'experience') {
                    if(section.data.entries.length){
                        if(section.data.type == 'education'){
                            education = section;
                            elements.push(React.createElement(Experience, {key: index, experience: education, type: education.data.type}));
                        }else if(section.data.type == 'work'){
                            work = section;
                            elements.push(React.createElement(Experience, {key: index, experience: work, type: work.data.type}));
                        }
                    }
                } else if (section.type == 'skill') {
                    skills= section;
                    elements.push(React.createElement(Skills, {key: index, skills: skills}));
                }
            });
            return  React.createElement("div", {id: "main", role: "main", class: "content"}, React.createElement(General, {general: general}), elements);

        }else{
            return React.createElement("p", null, "Loading");
        }
    }
});

ReactDOM.render(
    React.createElement(Resume, {url: "resume.json"}),
    document.getElementById('container')
);
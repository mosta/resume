/** @jsx React.DOM */
var Experience = React.createClass({displayName: "Experience",

    propTypes: {
        experience: React.PropTypes.object
    },

    showExperience: function() {
        var entries = [];
        var type = this.props.experience.data.type;
        this.props.experience.data.entries.map(function(item, index) {
            entries.push(React.createElement(ExperienceEntry, {entry: item, type: type, key: index}));
        });
        return entries;
    },

    render: function() {
        return React.createElement("section", {id: "experience", className: this.props.experience.icon}, 
                React.createElement("h1", null, this.props.experience.name), 
                React.createElement("table", {className: "entrys-table"}, 
                    React.createElement("tbody", null, 
                        this.showExperience()
                    )
                )
            );

    }

});